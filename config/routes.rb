Rails.application.routes.draw do
  resources :order_items
  resources :orders
  root 'orders#index'
  resources :pizza_types
end
